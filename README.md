# ansible-training

1. Deploy

2. Vault encrypt


Install ansible lint

sudo yum install python-pip
sudo pip install ansible-lint
ansible-lint deploy.yml

ansible-playbook -i hosts.yml deploy.yml*

ansible-playbook -i hosts.yml --ask-vault-pass deploy.yml

Ansible verbose option
ansible-playbook -i hosts.yml -vvv deploy.yml

ansible-vault encrypt files/secrets/credentials.yml

ssh-copy-id ubuntu@192.168.56.102

sudo apt install sshpass

ansible-galaxy collection install community.docker
ansible-galaxy collection list

ansible-galaxy install -r roles/requirements.yml